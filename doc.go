// Guapo is a collection of Go-packages that aim to make it easier to write tools for linux system administration.
//
// Similar to Ansible, Guapo executes commands on remote systems while trying to use as little dependencies on the
// remote host as possible. Commands can be executed over an SSH connection or another transport.
//
package guapo // import "go.sorcix.com/guapo"
