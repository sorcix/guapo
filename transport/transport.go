package transport // import "go.sorcix.com/guapo/transport"

import (
	"io"
)

// Transport describes a Guapo transport.
type Transport interface {

	// Returns the username on the remote end.
	User() string

	// Returns true if this transport is privileged on the remote end.
	IsPrivileged() bool

	Executer
	Copier

	io.Closer
}

// Executer describes functions to execute commands.
type Executer interface {

	// Execute runs a command on the remote end.
	Execute(command string, arg ...string) (err error)

	// Execute runs a command on the remote end and returns its output.
	ExecuteOutput(command string, arg ...string) (output []byte, err error)

	// ExecuteCombinedOutput runs a command on the remote end and returns stdout and stderr combined.
	ExecuteCombinedOutput(command string, arg ...string) (output []byte, err error)

	// ExecuteStream runs a command on the remote server.
	// Given reader and writers are connected to the process for as long as it runs.
	ExecuteStream(stdin io.Reader, stdout io.Writer, stderr io.Writer, command string, arg ...string) (err error)

	// Run runs given command on the remote end.
	Run(cmd *Command) (err error)
}

// Copier describes methods to copy files from and to the remote end.
type Copier interface {

	// CopyTo writes data to given file on the remote end.
	CopyTo(data io.Reader, path string) (err error)

	// CopyFrom reads data from give file on the remote end.
	CopyFrom(data io.Writer, path string) (err error)

	// Mkdir creates a directory on the remote end.
	Mkdir(path string) (err error)
}
