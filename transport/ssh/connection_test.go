package ssh_test

import (
	"os"
	"testing"

	"go.sorcix.com/guapo/transport/ssh"
)

var (
	testHost = os.Getenv("TEST_SSH_HOST")
	testUser = os.Getenv("TEST_SSH_USER")
)

func connect(t *testing.T) (conn *ssh.SecureShell) {
	if len(testHost) <= 0 || len(testUser) <= 0 {
		t.Log("Please set TEST_SSH_HOST and TEST_SSH_USER!")
		t.FailNow()
	}
	conn, err := ssh.DialWithAgent(testHost, testUser)
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	return conn
}

func TestDialWithAgent(t *testing.T) {
	conn := connect(t)
	err := conn.Close()
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
}
