package ssh

import (
	"net"
	"os"
	"sync"

	"golang.org/x/crypto/ssh/agent"
)

// The shared SSH agent connection.
//
// These variables should be accessed using the openAgent and closeAgent functions.
var (
	agentConn   net.Conn
	agentClient agent.Agent
	agentUsers  uint32
	agentMutex  sync.Mutex
	agentSock   = os.Getenv("SSH_AUTH_SOCK")
)

// openAgent returns a connection to the shared SSH agent.
//
// Multiple goroutines can simultaneously ask for a shared SSH agent connection. When the goroutine
// no longer needs the connection, it must call the closeAgent function.
func openAgent() (client agent.Agent, err error) {
	agentMutex.Lock()
	defer agentMutex.Unlock()

	// Check if can use an existing connection first.
	if agentClient == nil {
		if len(agentSock) < 1 {
			return nil, ErrNoAgent
		}

		if agentConn, err = net.Dial("unix", agentSock); err != nil {
			return nil, err
		}

		agentClient = agent.NewClient(agentConn)
	}

	// Add this user to the agent counter
	agentUsers++

	return agentClient, nil
}

// closeAgent marks a connection to the shared SSH agent as no longer needed.
func closeAgent() (err error) {
	agentMutex.Lock()
	defer agentMutex.Unlock()

	// Remove a user from the agent counter
	agentUsers--

	// Check if we have to close the shared connection
	if agentUsers < 1 {
		agentClient = nil
		if err = agentConn.Close(); err != nil {
			return err
		}
		agentConn = nil
	}

	return nil
}
