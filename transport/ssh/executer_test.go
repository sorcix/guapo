package ssh_test

import "testing"

func TestExecute(t *testing.T) {
	conn := connect(t)
	if err := conn.Execute("echo"); err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	if err := conn.Execute("/bin/false"); err == nil {
		t.Errorf("Expecting an error because of a non-nil return code!", err)
	}
}

func TestExecuteOutput(t *testing.T) {
	conn := connect(t)
	output, err := conn.ExecuteOutput("echo", "hello")
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	if string(output) != "hello\n" {
		t.Errorf("Expected hello but got '%s'.", string(output))
	}
}

func TestExecuteCombinedOutput(t *testing.T) {
	conn := connect(t)
	output, err := conn.ExecuteCombinedOutput("echo", "hello")
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	if string(output) != "hello\n" {
		t.Errorf("Expected hello but got '%s'.", string(output))
	}
	output, err = conn.ExecuteCombinedOutput("false")
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
}
