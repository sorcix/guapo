package ssh

import (
	"bytes"
	"fmt"
	"io"
	"strings"
	"sync"

	"go.sorcix.com/guapo/transport"
)

// Check interface implementation at compile time.
var _ transport.Executer = (*SecureShell)(nil)

// Execute runs a command on the remote host.
func (s *SecureShell) Execute(command string, arg ...string) (err error) {
	return s.Run(&transport.Command{
		Path: command,
		Args: arg,
	})
}

// ExecuteOutput runs a command on the remote host and returns its standard output.
func (s *SecureShell) ExecuteOutput(command string, arg ...string) (output []byte, err error) {
	var buffer bytes.Buffer
	err = s.Run(&transport.Command{
		Path:   command,
		Args:   arg,
		Stdout: &buffer,
	})
	return buffer.Bytes(), err
}

type singleWriter struct {
	buffer bytes.Buffer
	mutex  sync.Mutex
}

func (w *singleWriter) Write(data []byte) (n int, err error) {
	w.mutex.Lock()
	defer w.mutex.Unlock()
	return w.buffer.Write(data)
}

// ExecuteCombinedOutput runs a command on the remote host and returns its combined standard output and standard error.
func (s *SecureShell) ExecuteCombinedOutput(command string, arg ...string) (output []byte, err error) {
	var writer singleWriter
	err = s.Run(&transport.Command{
		Path:   command,
		Args:   arg,
		Stdout: &writer,
	})
	return writer.buffer.Bytes(), err
}

// ExecutePipe runs a command on the remote end and pipes its stdin and stdout.
func (s *SecureShell) ExecuteStream(stdin io.Reader, stdout io.Writer, stderr io.Writer, command string, arg ...string) (err error) {
	return s.Run(&transport.Command{
		Path:   command,
		Args:   arg,
		Stdin:  stdin,
		Stdout: stdout,
		Stderr: stderr,
	})
}

// Run executes given command on the remote host.
func (s *SecureShell) Run(cmd *transport.Command) (err error) {
	session, err := s.conn.NewSession()
	if err != nil {
		return fmt.Errorf("Unable to start an SSH session for command %s: %s", cmd.Path, err.Error())
	}

	// Patch through the input and output streams.
	session.Stdin = cmd.Stdin
	session.Stdout = cmd.Stdout
	session.Stderr = cmd.Stderr

	// Check if we have to set environment variables.
	if cmd.Env != nil {
		for key, value := range cmd.Env {
			session.Setenv(key, value)
		}
	}

	// The ssh package expects a single command string: concatenate everything.
	command := cmd.Path
	if len(cmd.Args) > 0 {
		command = command + " " + strings.Join(cmd.Args, " ")
	}

	// Attempt to start given command.
	if err = session.Run(command); err != nil {
		return fmt.Errorf("Unable to run command %s: %s", cmd.Path, err.Error())
	}
	return nil
}
