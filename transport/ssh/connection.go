package ssh

import (
	"errors"
	"time"

	"go.sorcix.com/guapo/transport"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

// Compile time check for the Transport interface.
var _ transport.Transport = (*SecureShell)(nil)

// Errors that may occur during use of the SecureShell transport.
var (
	ErrBadUser = errors.New("invalid username for SSH connection")
	ErrBadAuth = errors.New("unable to authenticate using given authentication options")
	ErrNoAgent = errors.New("unable to connect to SSH agent")
)

// Authentication lists possible authentication options for an SSH connection.
type Authentication struct {
	User     string
	Password string
	Key      []byte
	Agent    bool
}

// SecureShell is an SSH connection for Guapo.
type SecureShell struct {
	address string
	auth    *Authentication
	conn    *ssh.Client
	agent   agent.Agent
}

// Close disconnects from the SSH server and SSH agent.
func (s *SecureShell) Close() (err error) {
	if s.agent != nil {
		if err = closeAgent(); err != nil {
			return err
		}
		s.agent = nil
	}
	if s.conn != nil {
		if err = s.conn.Close(); err != nil {
			return err
		}
		s.conn = nil
	}
	return nil
}

// Dial connects to given SSH server.
//
// Address should be in the form host:port.
func Dial(address string, auth *Authentication) (s *SecureShell, err error) {
	s = &SecureShell{
		address: address,
		auth:    auth,
	}
	if err = s.Dial(); err != nil {
		return nil, err
	}
	return s, nil
}

// DialWithAgent connects to given SSH server and uses the SSH agent to log in.
func DialWithAgent(address, username string) (s *SecureShell, err error) {
	return Dial(address, &Authentication{User: username, Agent: true})
}

// Dial re-connects to a disconnected SecureShell transport.
func (s *SecureShell) Dial() (err error) {
	if s.auth == nil {
		return ErrBadAuth
	}

	methods := make([]ssh.AuthMethod, 0, 2)

	// Password authentication
	if len(s.auth.Password) > 1 {
		methods = append(methods, ssh.Password(s.auth.Password))
	}

	// Public key authentication
	if s.auth.Key != nil && len(s.auth.Key) > 10 {
		var key ssh.Signer
		if key, err = ssh.ParsePrivateKey(s.auth.Key); err != nil {
			return err
		}
		methods = append(methods, ssh.PublicKeys(key))
	}

	// Public key authentication using SSH agent
	if s.auth.Agent {
		if s.agent, err = openAgent(); err != nil {

			// We'll only complain about agent errors if we have no other way to authenticate.
			if len(methods) < 1 {
				return err
			}
		}

		methods = append(methods, ssh.PublicKeysCallback(s.agent.Signers))
	}

	if len(methods) < 1 {
		return ErrBadAuth
	}

	config := &ssh.ClientConfig{
		User:            s.auth.User,
		Auth:            methods,
		Timeout:         time.Minute,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	config.SetDefaults()

	config.Ciphers = append(config.Ciphers, "aes128-cbc", "3des-cbc")
	config.KeyExchanges = append(config.KeyExchanges, "diffie-hellman-group1-sha1")

	s.conn, err = ssh.Dial("tcp", s.address, config)

	return
}

// User returns the username on the remote end.
func (s *SecureShell) User() string {
	return s.auth.User
}

// IsPrivileged returns true if the user is privileged (root/administrator)
func (s *SecureShell) IsPrivileged() bool {
	return s.auth.User == "root"
}

// NewSession returns a golang.org/x/crypto/ssh.Session for this connection.
//
// This allows direct access to the underlying connection.
func (s *SecureShell) NewSession() (*ssh.Session, error) {
	return s.conn.NewSession()
}
