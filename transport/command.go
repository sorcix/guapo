package transport

import "io"

type Command struct {
	Path string
	Args []string
	Env  map[string]string

	Stdin  io.Reader
	Stdout io.Writer
	Stderr io.Writer
}
