module go.sorcix.com/guapo

go 1.13

require (
	golang.org/x/crypto v0.0.0-20180723164146-c126467f60eb
	golang.org/x/sys v0.0.0-20180801221139-3dc4335d56c7 // indirect
)
