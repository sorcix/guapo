package guapo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"go.sorcix.com/guapo/transport"
)

type Host struct {
	Hostname string    `json:"hostname"`
	FQDN     string    `json:"fqdn"`
	Boot     time.Time `json:"boot"`
	Network  *Network  `json:"network,omitempty"`
}

func (h *Host) JSON() ([]byte, error) {
	return json.MarshalIndent(h, "", "\t")
}

// Refresh updates host data using given transport.
func (h *Host) Refresh(conn transport.Transport) (err error) {
	var output []byte

	// Hostname
	if output, err = conn.ExecuteOutput("/bin/hostname"); err != nil {
		return err
	}
	output = bytes.TrimSpace(output)
	h.Hostname = string(output)

	// FQDN
	if output, err = conn.ExecuteOutput("/bin/hostname", "--fqdn"); err != nil {
		return err
	}
	output = bytes.TrimSpace(output)
	h.FQDN = string(output)

	// Uptime
	if output, err = conn.ExecuteOutput("/usr/bin/uptime", "--since"); err != nil {
		return err
	}
	output = bytes.TrimSpace(output)
	if h.Boot, err = time.Parse("2006-01-02 15:04:05", string(output)); err != nil {
		return fmt.Errorf("Unable to parse uptime: %s", string(output))
	}

	return h.Network.Refresh(conn)
}
