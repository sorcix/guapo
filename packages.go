package guapo

import "go.sorcix.com/guapo/transport"

type Package struct {
	Name string
}

type PackageManager interface {
	Install(pkg *Package) (err error)
	Remove(pkg string) (err error)
	Purge(pkg string) (err error)
	Update() (err error)
	Upgrade() (err error)
}

// NewPackageManager returns a PackageManager implementation that handles given remote host.
func NewPackageManager(tp transport.Transport) (pm PackageManager, err error) {
	return &Apt{
		tp: tp,
	}, nil
}

// Apt manages packages using apt-get (debian, ubuntu, ..) and implements the PackageManager interface.
type Apt struct {
	tp transport.Transport
}

const (
	aptGetPath   = "/usr/bin/apt-get"
	aptMarkPath  = "/usr/bin/apt-mark"
	aptCachePath = "/usr/bin/apt-cache"
)

func (a *Apt) run(path string, args []string) (err error) {
	return a.tp.Run(&transport.Command{
		Path: path,
		Args: args,
		Env: map[string]string{
			"DEBIAN_FRONTEND": "noninteractive",
		},
	})
}

// Install makes apt-get install given package.
func (a *Apt) Install(pkg *Package) (err error) {
	return a.run(aptGetPath, []string{
		"-q",
		"-y",
		"install",
		pkg.Name,
	})
}

// Remove makes apt-get remove given package.
func (a *Apt) Remove(pkg string) (err error) {
	return a.run(aptGetPath, []string{
		"-q",
		"-y",
		"remove",
		pkg,
	})
}

// Purge makes apt-get purge given package.
func (a *Apt) Purge(pkg string) (err error) {
	return a.run(aptGetPath, []string{
		"-q",
		"-y",
		"purge",
		pkg,
	})
}

// Lock makes apt-mark hold given package.
func (a *Apt) Lock(pkg string) (err error) {
	return a.run(aptMarkPath, []string{
		"hold",
		pkg,
	})
}

// Unlock makes apt-mark unhold given package.
func (a *Apt) Unlock(pkg string) (err error) {
	return a.run(aptMarkPath, []string{
		"unhold",
		pkg,
	})
}

// Update makes apt-get update package lists.
func (a *Apt) Update() (err error) {
	return a.run(aptGetPath, []string{
		"-q",
		"-y",
		"update",
	})
}

// Upgrade makes apt-get upgrade all packages.
func (a *Apt) Upgrade() (err error) {
	return a.run(aptGetPath, []string{
		"-q",
		"-y",
		"upgrade",
	})
}
