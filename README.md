# Guapo

Guapo is a collection of Go-packages that aim to make it easier to write
tools for linux system administration. Similar to [Ansible][], Guapo executes
commands on remote systems while trying to use as little dependencies on the
remote host as possible. Commands can be executed over an SSH connection or
another transport.

*Guapo is miniature configuration management for use in your own apps.*

[Ansible]: https://docs.ansible.com/ "Ansible documentation"
