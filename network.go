package guapo

import (
	"net"

	"go.sorcix.com/guapo/transport"
)

type Interface struct {
	Addresses []net.IP         `json:"addresses"`
	Mac       net.HardwareAddr `json:"mac"`
}

type Network struct {
	Interfaces []Interface `json:"interfaces"`
}

func (n *Network) Refresh(conn transport.Transport) (err error) {
	return
}
